<?php
    return [
        'user1' => [
            'idUser' => 1,
            'isTeacher' => 0,
            'FIO' => 'Ostoev Nikita Sergeevich',
            'login' => 'ostoev',
            'isDeleted' => 0,
            ], 
         'user2' => [
            'idUser' => 2,
            'isTeacher' => 1,
            'FIO' => 'Старышев Роман Игоревич',
            'login' => 'staryshe',
            'isDeleted' => 0,
            ], 
         'user3' => [
            'idUser' => 3,
            'isTeacher' => 0,
            'FIO' => 'Ермолаев Илья Владиславович',
            'login' => 'iermolae',
            'isDeleted' => 0,
            ],             
    ];