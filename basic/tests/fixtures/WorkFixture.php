<?php

namespace tests\fixtures;

use yii\test\ActiveFixture;

class WorkFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Work';
}