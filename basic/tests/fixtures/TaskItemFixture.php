<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class TaskItemFixture extends ActiveFixture
{
    public $modelClass = 'app\models\TaskItem';
}