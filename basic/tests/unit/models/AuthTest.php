<?php

namespace tests\unit\models;

use app\models\AuthForm;

class AuthFormTest extends \Codeception\Test\Unit
{
	protected $tester;
    private $model;

    public function testAuthCorrect()
    {
        $this->model = new AuthForm([
            'login' => 'staryshe',
            'pass' => 'eeloo2Ei',
        ]);

        expect_that($this->model->login());
    }

    public function testAuthIncorrect()
    {
        $this->model = new AuthForm([
            'login' => 'hlbtrlsg',
            'pass' => 'hlbtrlsg',
        ]);

        expect_not($this->model->login());
    }
}