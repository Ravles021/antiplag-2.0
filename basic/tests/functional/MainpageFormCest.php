<?php

use app\tests\fixtures\CourseFixture;
use app\tests\fixtures\UserFixture;

class MainpageFormCest
{

    protected $tester;


    public function _fixtures(){
        return ['tblCourse'=>CourseFixture::className(), 'tblUser'=>UserFixture::className()];
    }

    public function _before(\FunctionalTester $I)
    {
        session_save_path(yii::$app->basePath.'/sessions');
        $I->amOnRoute('auth/authr');
        $I->submitForm('#auth-form', [
            'AuthForm[login]' => 'staryshe',
            'AuthForm[pass]' => 'eeloo2Ei',
        ]);
        $I->amOnRoute('auth/mainpage');
    }


    public function openMainPage(\FunctionalTester $I){
    	$I->see('NameCourse1');
    }

    public function addCourse(\FunctionalTester $I){
    	$I->click('addCourse');
    	$I->see('Сохранить');
    }

    public function openCourse(\FunctionalTester $I){
    	$I->click('open_course_main_1');
    	$I->see('eto kurs odin');
    }
}