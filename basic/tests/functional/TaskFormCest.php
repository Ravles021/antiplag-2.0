<?php

use app\tests\fixtures\CourseFixture;
use app\tests\fixtures\UserFixture;
use app\tests\fixtures\TaskFixture;
use app\tests\fixtures\TaskItemFixture;

class TaskFormCest
{

    protected $tester;


    public function _fixtures(){
        return ['tblCourse'=>CourseFixture::className(), 'tblUser'=>UserFixture::className(), 'tblTask'=>TaskFixture::className(), 'tblTaskItem' => TaskItemFixture::className()];
    }

    public function _before(\FunctionalTester $I)
    {
        session_save_path(yii::$app->basePath.'/sessions');
        $I->amOnRoute('auth/authr');
        $I->submitForm('#auth-form', [
            'AuthForm[login]' => 'iermolae',
            'AuthForm[pass]' => 'eewoot3M',
        ]);
        $I->amOnRoute('auth/mainpage');
        $I->click('open_course_main_1');
        $I->click('open_task_course_1');
    }

    public function uploadFile(\FunctionalTester $I){
        $I->attachFile('input[type="file"]', '../files/1.php');
        $I->click('upload_taskItems1');
        $I->see('1.php');
    }

    public function uploadIncorrectFile(\FunctionalTester $I){
        $I->attachFile('input[type="file"]', '../files/1.txt');
        $I->click('upload_taskItems1');
        $I->dontSee('1.php');
        $I->see('Можно загружать только php файлы');
    }

    
}