<?php

use app\tests\fixtures\CourseFixture;
use app\tests\fixtures\UserFixture;
use app\tests\fixtures\TaskFixture;
use app\tests\fixtures\TaskItemFixture;

class TeacherTaskFormCest
{

    protected $tester;


    public function _fixtures(){
        return ['tblCourse'=>CourseFixture::className(), 'tblUser'=>UserFixture::className(), 'tblTask'=>TaskFixture::className(), 'tblTaskItem' => TaskItemFixture::className()];
    }

    public function _before(\FunctionalTester $I)
    {
        session_save_path(yii::$app->basePath.'/sessions');
        $I->amOnRoute('auth/authr');
        $I->submitForm('#auth-form', [
            'AuthForm[login]' => 'staryshe',
            'AuthForm[pass]' => 'eeloo2Ei',
        ]);
        $I->amOnRoute('auth/mainpage');
        $I->click('open_course_main_1');
        $I->click('open_task_course_1');
    }


    public function openEditTask(\FunctionalTester $I){
        $I->click('edit_task_task_1');
        $I->see('Сохранить');
    }

    public function deleteTask(\FunctionalTester $I){
        $I->click('delete_task_task_1');
        $I->dontSee('Task1');
    }

    public function openReport(\FunctionalTester $I){
        $I->click('open_report_task');
        $I->see('Task1');
    }

    public function EditTask(\FunctionalTester $I){
        $I->click('edit_task_task_1');
        $I->submitForm('#edit-task-form', [
            'EditTaskForm[task_uniqueness]' => '70',
        ]);
        $I->click('save_task_edittask_1');
        $I->see('Уникальность: 70%');
    }
}