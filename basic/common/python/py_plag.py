#!/usr/bin/env python3
import nltk

def sravniFiles(strfile, strfile2):
    def is_digit(n):
        try:
            int(n)
            return True
        except ValueError:
            return False

    f = open(strfile, "r", encoding="utf-8")
    raws = f.readlines()

    string = ''

    for raw in raws:
        if '#' in raw:
            string += raw.split('#')[0]
            if raw[0] != '#':
                string += '\n'
        elif raw[0:6] != 'import' and raw[0:4] != 'from' and raw != '\n':
            string += raw


    strings = ''

    j = 0
    i = 0

    while i <= len(string):
        i += 1

    tokens = nltk.word_tokenize(string)

    # CHISLO - 1 - числовое значение/сивол/набор символов ! ГОТОВО
    # IDEN - 2 - идентификатор ($per) ! ГОТОВО
    # KL - 3 - ключевое слово языка ! ГОТОВО
    # OPER - 4 - операции ! ГОТОВО
    # VHOD - 5 - вхождение ! ГОТОВО
    # SKOB - 6 - скобка ! ГОТОВО



    operators = []
    i = 0
    while i < len(tokens):
        if tokens[i] == "'" or tokens[i] == "`" or tokens[i].startswith("'") or tokens[i].startswith("`"):
            tokens[i] = 'CHISLO'
            i += 1
            while i < len(tokens):
                if tokens[i] == "'" or tokens[i] == "`" or tokens[i].endswith("'") or tokens[i].endswith("`"):
                    del tokens[i]
                    break
                else:
                    del tokens[i]
        elif tokens[i] == "''" or tokens[i] == "``":
            tokens[i] = 'CHISLO'
            i += 1
            while i < len(tokens):
                if tokens[i] == "''" or tokens[i] == "``":
                    del tokens[i]
                    break
                else:
                    del tokens[i]
        elif tokens[i] == '"':
            tokens[i] = 'CHISLO'
            i += 1
            while i < len(tokens):
                if tokens[i] == '"':
                    del tokens[i]
                    break
                else:
                    del tokens[i]
        elif is_digit(tokens[i]) or tokens[i].casefold() == "true" or tokens[i].casefold() == 'false' or tokens[i].casefold() == 'None':
            tokens[i] = 'CHISLO'
            i += 1
        elif i < (len(tokens)-1) and tokens[i + 1] == '=' and tokens[i] != ']':
            operators.append(tokens[i])
            tokens[i] = 'IDEN'
            tokens[i + 1] = 'OPER'
            i += 2

        elif tokens[i] in operators:
            tokens[i] = 'IDEN'
            i += 1


        elif i < (len(tokens) - 1) and i < (len(tokens)-1) and tokens[i][len(tokens[i]) - 1] == '-' and tokens[i + 1] == '>':
            tokens[i] = 'IDEN'
            tokens[i + 1] = 'OPER'
            i += 2
        elif tokens[i] == ':':
            tokens[i] = 'VHOD'
            i += 1
        elif tokens[i] == '+' or tokens[i] == '-' or tokens[i] == '*' or tokens[i] == '**' or tokens[i] == '/' or \
                tokens[i] == '%' or tokens[
            i] == '|' or tokens[i] == '||' or tokens[i] == '&' or tokens[i] == '&&' or tokens[i] == '==' or tokens[
            i] == '>=' or tokens[i] == '<=' or tokens[i] == '^' or tokens[i] == '!' or tokens[i] == '+=' or tokens[
            i] == '-=' or tokens[i] == '*=' or tokens[i] == '/=' or tokens[
            i] == '&=' or tokens[i] == '|=' or tokens[i] == '=' or tokens[i] == 'or' or tokens[i] == 'and':
            tokens[i] = 'OPER'
            i += 1
        elif tokens[i] == '<' or tokens[i] == '>':
            if tokens[i + 1] == '<' or tokens[i + 1] == '>':
                del tokens[i]
                tokens[i] = 'OPER'
                i += 1
            else:
                tokens[i] = 'OPER'
                i += 1
        elif tokens[i] == '(' or tokens[i] == ')' or tokens[i] == '{' or tokens[i] == '}' or tokens[i] == '[' or tokens[
            i] == ']':
            tokens[i] = 'SKOB'
            i += 1
        else:
            tokens[i] = 'KL'
            i += 1

    print(tokens)

    for i in range(0, len(tokens)):
        if tokens[i] == 'CHISLO':
            tokens[i] = '1'
        elif tokens[i] == 'IDEN':
            tokens[i] = '2'
        elif tokens[i] == 'KL':
            tokens[i] = '3'
        elif tokens[i] == 'OPER':
            tokens[i] = '4'
        elif tokens[i] == 'VHOD':
            tokens[i] = '5'
        elif tokens[i] == 'SKOB':
            tokens[i] = '6'

    grafs = [0] * (len(tokens) - 3)

    i = 0
    while i + 3 < len(tokens):
        grafs[i] = tokens[i] + tokens[i + 1] + tokens[i + 2] + tokens[i + 3]
        i += 1

    i = 0
    while i < len(grafs) - 1:
        j = 0
        while j < len(grafs):
            if grafs[i] == grafs[j] and (i != j):
                del grafs[j]
                j -= 1
            j += 1
        i += 1

    f.close()

    f = open(strfile2, "r", encoding="utf-8")
    raws = f.readlines()

    string = ''

    for raw in raws:
        if '#' in raw:
            string += raw.split('#')[0]
            if raw[0] != '#':
                string += '\n'
        elif raw[0:6] != 'import' and raw[0:4] != 'from' and raw != '\n':
            string += raw


    strings = ''

    j = 0
    i = 0

    while i <= len(string):
        i += 1

    tokens = nltk.word_tokenize(string)

    # CHISLO - 1 - числовое значение/сивол/набор символов ! ГОТОВО
    # IDEN - 2 - идентификатор ($per) ! ГОТОВО
    # KL - 3 - ключевое слово языка ! ГОТОВО
    # OPER - 4 - операции ! ГОТОВО
    # VHOD - 5 - вхождение ! ГОТОВО
    # SKOB - 6 - скобка ! ГОТОВО



    operators = []
    i = 0
    while i < len(tokens):
        if tokens[i] == "'" or tokens[i] == "`" or tokens[i].startswith("'") or tokens[i].startswith("`"):
            tokens[i] = 'CHISLO'
            i += 1
            while i < len(tokens):
                if tokens[i] == "'" or tokens[i] == "`" or tokens[i].endswith("'") or tokens[i].endswith("`"):
                    del tokens[i]
                    break
                else:
                    del tokens[i]
        elif tokens[i] == "''" or tokens[i] == "``":
            tokens[i] = 'CHISLO'
            i += 1
            while i < len(tokens):
                if tokens[i] == "''" or tokens[i] == "``":
                    del tokens[i]
                    break
                else:
                    del tokens[i]
        elif tokens[i] == '"':
            tokens[i] = 'CHISLO'
            i += 1
            while i < len(tokens):
                if tokens[i] == '"':
                    del tokens[i]
                    break
                else:
                    del tokens[i]
        elif is_digit(tokens[i]) or tokens[i].casefold() == "true" or tokens[i].casefold() == 'false' or tokens[
            i].casefold() == 'None':
            tokens[i] = 'CHISLO'
            i += 1
        elif i < (len(tokens)-1) and tokens[i + 1] == '=' and tokens[i] != ']':
            operators.append(tokens[i])
            tokens[i] = 'IDEN'
            tokens[i + 1] = 'OPER'

        elif tokens[i] in operators:
            tokens[i] = 'IDEN'
            i += 1


        elif tokens[i][len(tokens[i]) - 1] == '-' and tokens[i + 1] == '>':
            tokens[i] = 'IDEN'
            tokens[i + 1] = 'OPER'
            i += 2
        elif tokens[i] == ':':
            tokens[i] = 'VHOD'
            i += 1
        elif tokens[i] == '+' or tokens[i] == '-' or tokens[i] == '*' or tokens[i] == '**' or tokens[i] == '/' or \
                tokens[i] == '%' or tokens[
            i] == '|' or tokens[i] == '||' or tokens[i] == '&' or tokens[i] == '&&' or tokens[i] == '==' or tokens[
            i] == '>=' or tokens[i] == '<=' or tokens[i] == '^' or tokens[i] == '!' or tokens[i] == '+=' or tokens[
            i] == '-=' or tokens[i] == '*=' or tokens[i] == '/=' or tokens[
            i] == '&=' or tokens[i] == '|=' or tokens[i] == '=' or tokens[i] == 'or' or tokens[i] == 'and':
            tokens[i] = 'OPER'
            i += 1
        elif i < (len(tokens)-1) and tokens[i] == '<' or tokens[i] == '>':
            if tokens[i + 1] == '<' or tokens[i + 1] == '>':
                del tokens[i]
                tokens[i] = 'OPER'
                i += 1
            else:
                tokens[i] = 'OPER'
                i += 1
        elif tokens[i] == '(' or tokens[i] == ')' or tokens[i] == '{' or tokens[i] == '}' or tokens[i] == '[' or tokens[
            i] == ']':
            tokens[i] = 'SKOB'
            i += 1
        else:
            tokens[i] = 'KL'
            i += 1

    for i in range(0, len(tokens)):
        if tokens[i] == 'CHISLO':
            tokens[i] = '1'
        elif tokens[i] == 'IDEN':
            tokens[i] = '2'
        elif tokens[i] == 'KL':
            tokens[i] = '3'
        elif tokens[i] == 'OPER':
            tokens[i] = '4'
        elif tokens[i] == 'VHOD':
            tokens[i] = '5'
        elif tokens[i] == 'SKOB':
            tokens[i] = '6'

    grafssrav = [0] * (len(tokens) - 3)

    i = 0
    while i + 3 < len(tokens):
        grafssrav[i] = tokens[i] + tokens[i + 1] + tokens[i + 2] + tokens[i + 3]
        i += 1

    i = 0
    while i < len(grafssrav) - 1:
        j = 0
        while j < len(grafssrav):
            if grafssrav[i] == grafssrav[j] and (i != j):
                del grafssrav[j]
                j -= 1
            j += 1
        i += 1

    koef = 0
    i = 0
    while i < len(grafs) - 1:
        j = 0
        while j < len(grafssrav):
            if grafs[i] == grafssrav[j]:
                koef += 1
            j += 1
        i += 1

    k = len(grafs)
    original = 100 - ((koef / (k - 1)) * 100)

    return original


# РАСКОММЕНТИТЬ 2 СТРОЧКИ СНИЗУ И УБРАТЬ ПОСЛЕДНЮЮ!!!

# nltk.download('punkt')
# print(sravniFiles(sys.argv[1], sys.argv[2]))

print(sravniFiles('E:\\python\\labsPy\\1file.py', 'E:\\python\\labsPy\\2file.py'))
