#!/usr/bin/env python3
import sys
import nltk


def sravniFiles(strfile, strfile2):

    def is_digit(n):
        try:
            int(n)
            return True
        except ValueError:
            return False

    f = open(strfile, "r", encoding="utf-8")
    raws = f.readlines()

    string = ''

    for raw in raws:
        if '//' in raw and '*/' not in raw:
            string += raw.split("//")[0]
        elif raw[0:9] != 'namespace' and raw[0:5] != 'using' and raw != '\n':
            string += raw


    strings = ''

    j = 0
    i = 0
    while i <= len(string):
        if i == len(string):
            k = i
            if '/*' in string[j:k]:
                string = string.split("/*")[0]
            strings += string[j:k]
            break
        elif i <= len(string) - 2 and string[i] == "/" and string[i + 1] == "*":
            k = i
            i += 2
            strings += string[j:k]
        elif i <= len(string) - 2 and string[i] == "*" and string[i + 1] == "/":
            j = i + 2
        i += 1

    tokens = nltk.word_tokenize(strings)

    # CHISLO - 1 - числовое значение/сивол/набор символов ! ГОТОВО
    # IDEN - 2 - идентификатор ($per) ! ГОТОВО
    # KL - 3 - ключевое слово языка ! ГОТОВО
    # OPER - 4 - операции ! ГОТОВО
    # PER - 5 - перенос строки ! ГОТОВО
    # SKOB - 6 - скобка ! ГОТОВО

    operators = []
    i = 0
    while i < len(tokens):
        if tokens[i] == "'" or tokens[i] == "`" or tokens[i].startswith("'") or tokens[i].startswith("`"):
            tokens[i] = 'CHISLO'
            i += 1
            while i < len(tokens):
                if tokens[i] == "'" or tokens[i] == "`" or tokens[i].endswith("'") or tokens[i].endswith("`"):
                    del tokens[i]
                    break
                else:
                    del tokens[i]
        elif tokens[i] == "''" or tokens[i] == "``":
            tokens[i] = 'CHISLO'
            i += 1
            while i < len(tokens):
                if tokens[i] == "''" or tokens[i] == "``":
                    del tokens[i]
                    break
                else:
                    del tokens[i]
        elif tokens[i] == '"':
            tokens[i] = 'CHISLO'
            i += 1
            while i < len(tokens):
                if tokens[i] == '"':
                    del tokens[i]
                    break
                else:
                    del tokens[i]
        elif is_digit(tokens[i]) or tokens[i].casefold() == "true" or tokens[i].casefold() == 'false':
            tokens[i] = 'CHISLO'
            i += 1
        elif tokens[i].casefold() == "signed" or tokens[i].casefold() == "unsigned":
            del tokens[i]
        elif tokens[i].casefold() == "long" and tokens[i + 1].casefold() == "long" and tokens[
            i + 2].casefold() == "int":
            del tokens[i]
            del tokens[i]
            del tokens[i]
            operators.append(tokens[i])
            tokens[i] = 'OPER'
            i += 1
        elif tokens[i].casefold() == "long" and tokens[i + 1].casefold() == "long" and tokens[
            i + 2].casefold() != "int":
            del tokens[i]
            del tokens[i]
            operators.append(tokens[i])
            tokens[i] = 'OPER'
            i += 1
        elif tokens[i].casefold() == "long" or tokens[i].casefold() == "short":
            del tokens[i]
        elif tokens[i].casefold() == "char" or tokens[i].casefold() == 'int' or tokens[i].casefold() == 'string' or \
                tokens[i].casefold() == 'float' or tokens[i].casefold() == 'double' or tokens[i].casefold() == 'long' or \
                tokens[i].casefold() == 'bool':
            if tokens[i + 2] == '[' and (tokens[i + 3] == ']' or tokens[i + 4] == ']'):
                del tokens[i]
                operators.append(tokens[i])
                tokens[i] = 'IDEN'
                i += 1
                while tokens[i] == '[' and (tokens[i + 1] == ']' or tokens[i + 2] == ']'):
                    tokens[i] = 'SKOB'
                    i += 1
                    if is_digit(tokens[i]):
                        tokens[i] = 'CHISLO'
                        i += 1
                    elif tokens[i] in operators:
                        tokens[i] = 'IDEN'
                        i += 1
                    tokens[i] = 'SKOB'
                    i += 1
            else:
                del tokens[i]
                operators.append(tokens[i])
                tokens[i] = 'IDEN'
                i += 1
        elif i != len(tokens) - 1 and tokens[i + 1] == '=' and tokens[i + 2] == 'new':
            operators.append(tokens[i])
            tokens[i] = 'IDEN'
            i += 1
        elif tokens[i].casefold() == 'struct' or tokens[i].casefold() == 'enum' or tokens[i].casefold() == 'class' \
                or tokens[i].casefold() == 'interface':
            operators.append(tokens[i + 1])
            tokens[i] = 'KL'
            i += 1
        elif tokens[i] in operators:
            tokens[i] = 'IDEN'
            i += 1
        elif tokens[i][len(tokens[i]) - 1] == '-' and tokens[i + 1] == '>':
            tokens[i] = 'IDEN'
            tokens[i + 1] = 'OPER'
            i += 2
        elif tokens[i] == ';':
            tokens[i] = 'PER'
            i += 1
        elif tokens[i] == '+' or tokens[i] == '-' or tokens[i] == '*' or tokens[i] == '/' or tokens[i] == '%' or tokens[
            i] == '|' or tokens[i] == '||' or tokens[i] == '&' or tokens[i] == '&&' or tokens[i] == '==' or tokens[
            i] == '>=' or tokens[i] == '<=' or tokens[i] == '^' or tokens[i] == '!' or tokens[i] == '+=' or tokens[
            i] == '-=' or tokens[i] == '*=' or tokens[i] == '/=' or tokens[
            i] == '&=' or tokens[i] == '|=' or tokens[i] == '=':
            tokens[i] = 'OPER'
            i += 1
        elif tokens[i] == '<' or tokens[i] == '>':
            if tokens[i + 1] == '<' or tokens[i + 1] == '>':
                del tokens[i]
                tokens[i] = 'OPER'
                i += 1
            else:
                tokens[i] = 'OPER'
                i += 1
        elif tokens[i] == '(' or tokens[i] == ')' or tokens[i] == '{' or tokens[i] == '}' or tokens[i] == '[' or tokens[i] == ']':
            tokens[i] = 'SKOB'
            i += 1
        else:
            tokens[i] = 'KL'
            i += 1

    for i in range(0, len(tokens)):
        if tokens[i] == 'CHISLO':
            tokens[i] = '1'
        elif tokens[i] == 'IDEN':
            tokens[i] = '2'
        elif tokens[i] == 'KL':
            tokens[i] = '3'
        elif tokens[i] == 'OPER':
            tokens[i] = '4'
        elif tokens[i] == 'PER':
            tokens[i] = '5'
        elif tokens[i] == 'SKOB':
            tokens[i] = '6'

    grafs = [0] * (len(tokens) - 3)

    i = 0
    while i + 3 < len(tokens):
        grafs[i] = tokens[i] + tokens[i + 1] + tokens[i + 2] + tokens[i + 3]
        i += 1

    i = 0
    while i < len(grafs) - 1:
        j = 0
        while j < len(grafs):
            if grafs[i] == grafs[j] and (i != j):
                del grafs[j]
                j -= 1
            j += 1
        i += 1

    f.close()

    f = open(strfile2, "r", encoding="utf-8")
    raws = f.readlines()

    string = ''

    for raw in raws:
        if '//' in raw and '*/' not in raw:
            string += raw.split("//")[0]
        elif raw[0:7] != 'package' and raw[0:6] != 'import' and raw[0:20] != 'using namespace std;' and raw != '\n':
            string += raw

    strings = ''

    j = 0
    i = 0
    while i <= len(string):
        if i == len(string):
            k = i
            if '/*' in string[j:k]:
                string = string.split("/*")[0]
            strings += string[j:k]
            break
        elif i <= len(string) - 2 and string[i] == "/" and string[i + 1] == "*":
            k = i
            i += 2
            strings += string[j:k]
        elif i <= len(string) - 2 and string[i] == "*" and string[i + 1] == "/":
            j = i + 2
        i += 1

    tokens = nltk.word_tokenize(strings)

    # CHISLO - 1 - числовое значение/сивол/набор символов ! ГОТОВО
    # IDEN - 2 - идентификатор ($per) ! ГОТОВО
    # KL - 3 - ключевое слово языка ! ГОТОВО
    # OPER - 4 - операции ! ГОТОВО
    # PER - 5 - перенос строки ! ГОТОВО
    # SKOB - 6 - скобка ! ГОТОВО

    operators = []
    i = 0
    while i < len(tokens):
        if tokens[i] == "'" or tokens[i] == "`" or tokens[i].startswith("'") or tokens[i].startswith("`"):
            tokens[i] = 'CHISLO'
            i += 1
            while i < len(tokens):
                if tokens[i] == "'" or tokens[i] == "`" or tokens[i].endswith("'") or tokens[i].endswith("`"):
                    del tokens[i]
                    break
                else:
                    del tokens[i]
        elif tokens[i] == "''" or tokens[i] == "``":
            tokens[i] = 'CHISLO'
            i += 1
            while i < len(tokens):
                if tokens[i] == "''" or tokens[i] == "``":
                    del tokens[i]
                    break
                else:
                    del tokens[i]
        elif tokens[i] == '"':
            tokens[i] = 'CHISLO'
            i += 1
            while i < len(tokens):
                if tokens[i] == '"':
                    del tokens[i]
                    break
                else:
                    del tokens[i]
        elif is_digit(tokens[i]) or tokens[i].casefold() == "true" or tokens[i].casefold() == 'false':
            tokens[i] = 'CHISLO'
            i += 1
        elif tokens[i].casefold() == "signed" or tokens[i].casefold() == "unsigned":
            del tokens[i]
        elif tokens[i].casefold() == "long" and tokens[i + 1].casefold() == "long" and tokens[
            i + 2].casefold() == "int":
            del tokens[i]
            del tokens[i]
            del tokens[i]
            operators.append(tokens[i])
            tokens[i] = 'OPER'
            i += 1
        elif tokens[i].casefold() == "long" and tokens[i + 1].casefold() == "long" and tokens[
            i + 2].casefold() != "int":
            del tokens[i]
            del tokens[i]
            operators.append(tokens[i])
            tokens[i] = 'OPER'
            i += 1
        elif tokens[i].casefold() == "long" or tokens[i].casefold() == "short":
            del tokens[i]
        elif tokens[i].casefold() == "char" or tokens[i].casefold() == 'int' or tokens[i].casefold() == 'string' or \
                tokens[i].casefold() == 'float' or tokens[i].casefold() == 'double' or tokens[i].casefold() == 'long' or \
                tokens[i].casefold() == 'bool':
            if tokens[i + 2] == '[' and (tokens[i + 3] == ']' or tokens[i + 4] == ']'):
                del tokens[i]
                operators.append(tokens[i])
                tokens[i] = 'IDEN'
                i += 1
                while tokens[i] == '[' and (tokens[i + 1] == ']' or tokens[i + 2] == ']'):
                    tokens[i] = 'SKOB'
                    i += 1
                    if is_digit(tokens[i]):
                        tokens[i] = 'CHISLO'
                        i += 1
                    elif tokens[i] in operators:
                        tokens[i] = 'IDEN'
                        i += 1
                    tokens[i] = 'SKOB'
                    i += 1
            else:
                del tokens[i]
                operators.append(tokens[i])
                tokens[i] = 'IDEN'
                i += 1
        elif i != len(tokens) - 1 and tokens[i + 1] == '=' and tokens[i + 2] == 'new':
            operators.append(tokens[i])
            tokens[i] = 'IDEN'
            i += 1
        elif tokens[i].casefold() == 'struct' or tokens[i].casefold() == 'enum' or tokens[i].casefold() == 'class' \
                or tokens[i].casefold() == 'interface':
            operators.append(tokens[i + 1])
            tokens[i] = 'KL'
            i += 1
        elif tokens[i] in operators:
            tokens[i] = 'IDEN'
            i += 1
        elif tokens[i][len(tokens[i]) - 1] == '-' and tokens[i + 1] == '>':
            tokens[i] = 'IDEN'
            tokens[i + 1] = 'OPER'
            i += 2
        elif tokens[i] == ';':
            tokens[i] = 'PER'
            i += 1
        elif tokens[i] == '+' or tokens[i] == '-' or tokens[i] == '*' or tokens[i] == '/' or tokens[i] == '%' or tokens[
            i] == '|' or tokens[i] == '||' or tokens[i] == '&' or tokens[i] == '&&' or tokens[i] == '==' or tokens[
            i] == '>=' or tokens[i] == '<=' or tokens[i] == '^' or tokens[i] == '!' or tokens[i] == '+=' or tokens[
            i] == '-=' or tokens[i] == '*=' or tokens[i] == '/=' or tokens[
            i] == '&=' or tokens[i] == '|=' or tokens[i] == '=':
            tokens[i] = 'OPER'
            i += 1
        elif tokens[i] == '<' or tokens[i] == '>':
            if tokens[i + 1] == '<' or tokens[i + 1] == '>':
                del tokens[i]
                tokens[i] = 'OPER'
                i += 1
            else:
                tokens[i] = 'OPER'
                i += 1
        elif tokens[i] == '(' or tokens[i] == ')' or tokens[i] == '{' or tokens[i] == '}' or tokens[i] == '[' or tokens[
            i] == ']':
            tokens[i] = 'SKOB'
            i += 1
        else:
            tokens[i] = 'KL'
            i += 1

    for i in range(0, len(tokens)):
        if tokens[i] == 'CHISLO':
            tokens[i] = '1'
        elif tokens[i] == 'IDEN':
            tokens[i] = '2'
        elif tokens[i] == 'KL':
            tokens[i] = '3'
        elif tokens[i] == 'OPER':
            tokens[i] = '4'
        elif tokens[i] == 'PER':
            tokens[i] = '5'
        elif tokens[i] == 'SKOB':
            tokens[i] = '6'

    grafssrav = [0] * (len(tokens) - 3)

    i = 0
    while i + 3 < len(tokens):
        grafssrav[i] = tokens[i] + tokens[i + 1] + tokens[i + 2] + tokens[i + 3]
        i += 1

    i = 0
    while i < len(grafssrav) - 1:
        j = 0
        while j < len(grafssrav):
            if grafssrav[i] == grafssrav[j] and (i != j):
                del grafssrav[j]
                j -= 1
            j += 1
        i += 1

    koef = 0
    i = 0
    while i < len(grafs) - 1:
        j = 0
        while j < len(grafssrav):
            if grafs[i] == grafssrav[j]:
                koef += 1
            j += 1
        i += 1

    k = len(grafs)
    original = 100 - ((koef / (k - 1)) * 100)

    return original


# РАСКОММЕНТИТЬ 2 СТРОЧКИ СНИЗУ И УБРАТЬ ПОСЛЕДНЮЮ!!!

# nltk.download('punkt')
# print(sravniFiles(sys.argv[1], sys.argv[2]))

print(sravniFiles('E:\\python\\labsPy\\1file.cpp', 'E:\\python\\labsPy\\2file.cpp'))
