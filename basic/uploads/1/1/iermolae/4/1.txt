1st Block: 

1. Имя: VNXe-H1181 (На Главной)

2. Версия ПО: 3.0.0 2585212 (Система -> Состояние системы)

3. Количество памяти доступно: 89.30% (Система -> Системные ресурсы)

4. Выделено: 10.70%

5. Выделено под общие папки: Памяти выделено под общие папки: 2.87%

6. Типы: Информация, предупреждение (На Главной)

7. Flash (Система -> Состояние системы)

8. Порты: (Система -> Состояние системы -> SP A I/O Module 0)
SP A I/O Module 0 FC Port 0
SP A I/O Module 0 FC Port 1
SP A I/O Module 0 FC Port 2
SP A I/O Module 0 FC Port 3










2nd Block: 

1. Файловые системы: (Система хранения данных -> FileSystems -> Имя, протокол, размер) 
FileSystem00, CIFS, 2TB
FileSystem00, NFS, 1TB

2. LUN-ы (Система хранения данных -> LUNs)
LUN00, 250gb
LUNGroup-FC, 300gb
	LUNGroup-FC-00, 200.0gb
	LUNGroup-FC-01, 100.0gb
LUNGroup-iSCSI, 2TB
	LUNGroup-iSCSI-00, 512.0gb
	LUNGroup-iSCSI-01, 512.0gb
	LUNGroup-iSCSI-02, 512.0gb
	LUNGroup-iSCSI-03, 512.0gb

3. Пул хранения: (Система хранения данных -> Storage Configuration -> Пулы хранения)
MultiTier - 10 дисков

4. FAST VP: (Система хранения данных -> Storage Configuration -> Пулы хранения -> Сведения -> FAST VP)
Поддерживает

5. Доступных резервных дисков: (Система хранения данных -> Storage Configuration -> Spare Disks) 
24










3rd Block: 
1. Категории: (Хосты) 
Хосты, VMWare, Initiators


2. Доступные хосты: (Хосты -> Хосты) 
10.244.214.222, FC, VMware ESXi 5.1.0
10.244.238.55, iSCSI, VMware ESXi 5.0.0
Win7, iSCSI, Windows 7
Windows 2k8, iSCSI, Windows Server 2008

3. Инициаторы не связаные с хостом: (Хосты -> Initiators)
20:00:00:90:FA:14:3F:10:10:00:00:90:FA:14:3F:10
20:00:00:90:FA:14:3F:11:10:00:00:90:FA:14:3F:11


4. Экземпляторы связанные с массивом, их имена, версии ESXi системы, количество
виртуальных машин: (Хосты -> VMware)

10.244.237.159, VMware ESXi 5.0.0, 4
10.244.214.223, VMware ESXi 5.1.0, 7












