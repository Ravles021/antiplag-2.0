<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\AuthForm;
use app\models\EditCourseForm;
use app\models\TblUser;
use app\models\Selected;
use app\models\Task;
use app\models\TaskItem;
use app\models\Work;
use app\models\WorkItem;
use yii\widgets\LinkPager;
use yii\helpers\Html;
use app\models\Course;

class CourseController extends Controller
{
    public function actionCourse($id=null){
        if (isset($_SESSION['auth'])){
            if(!isset($id)){
                return $this->redirect(["auth/mainpage"]);
            }

            $course = Course::findOne($id);
            $task_exist = Task::find()->where(['idCourse'=>$course['idCourse'], 'isDeleted'=>0])->exists();
            $tasks = Task::find()->where(['idCourse'=>$course['idCourse'], 'isDeleted'=>0])->all();
            $versions = [];
            $color = [];
            $percent = [];
            if(isset($_SESSION['status'])){
                $usr = TblUser::findOne(['Login'=>$_SESSION['auth']]);
                foreach($tasks as &$row){
                    if(Work::find()->where(['isDeleted'=>0, 'idTask'=>$row['idTask'], 'idUser'=>$usr['idUser']])->exists()){ 
                        $version = Work::find()->where(['isDeleted'=>0, 'idTask'=>$row['idTask'], 'idUser'=>$usr['idUser']])->count();
                        $versions[$row['idTask']] = $version;
                        $last_version = Work::find()->where(['isDeleted'=>0, 'idTask'=>$row['idTask'], 'idUser'=>$usr['idUser']])->orderBy(['Version' => SORT_DESC])->one();
                        if($last_version['Uniqueness'] >=$row['Uniqueness']){
                            $color[$row['idTask']] = 'green';
                        }
                        elseif($last_version['Uniqueness'] < $row['Uniqueness'] && $last_version['Uniqueness'] >=0){
                            $color[$row['idTask']] = 'red';
                        }
                        elseif($last_version['Uniqueness'] ==-1){
                            $color[$row['idTask']] = 'yellow';
                        }
                    }
                    else {
                        $versions[$row['idTask']] = 0;
                        $color[$row['idTask']] = 'grey';
                    }
                }
            }
            else{
                foreach($tasks as &$row){
                    $studentsWorks = [];
                    $students = TblUser::find()->where(['isTeacher'=>False])->all();
                    foreach($students as &$student){
                        if(Work::find()->where(['isDeleted'=>0, 'idTask'=>$row['idTask'], 'idUser'=>$student['idUser']])->exists()){
                            //$studentsWorks[$student['idUser']] = Work::findOne(['isDeleted'=>0, 'idTask'=>$row['idTask'], 'idUser'=>$student['idUser']])->max('Date');
                            $studentsWorks[$student['idUser']] = Work::find()->where(['isDeleted'=>0, 'idTask'=>$row['idTask'], 'idUser'=>$student['idUser']])->orderBy(['Version' => SORT_DESC])->one();
                        }
                    }
                    //var_dump($studentsWorks);
                    //die();
                    $countOfWorks = count($studentsWorks);
                    $countUniqWorks = 0;
                    foreach($studentsWorks as &$works){
                        if($works['Uniqueness']>=$row['Uniqueness']){
                            $countUniqWorks ++;
                        }
                    }
                    $versions[$row['idTask']]= Work::find()->where(['isDeleted'=>0, 'idTask'=>$row['idTask']])->count();
                    if ($countOfWorks == 0){
                        $percent[$row['idTask']] = 100;
                    }
                    else{
                        $percent[$row['idTask']] = ($countUniqWorks/$countOfWorks)*100;
                    }
                }
            }
            if(Yii::$app->request->post('go_to_the_mainpage')){
                return $this->redirect(array('auth/mainpage'));
            }
	        if(Yii::$app->request->post('delete_course_course_'.$course['idCourse'])){
                $course -> isDeleted = 1;
                $course -> save();
                if (Task::find()->where(['idCourse'=>$course['idCourse']])->exists()){
                    $tasks = Task::find()->where(['idCourse'=>$course['idCourse']])->all();
                    Task::updateAll(['isDeleted' => 1], ['idCourse'=>$course['idCourse']]);
                    foreach($tasks as &$task){
                        TaskItem::updateAll(['isDeleted' => 1], ['idTask'=>$task['idTask']]);
                        if(Work::find()->where(['idTask'=>$task['idTask']])->exists()){
                            $works = Work::find()->where(['idTask'=>$task['idTask']])->all();
                            Work::updateAll(['isDeleted' => 1], ['idTask'=>$task['idTask']]);
                            foreach($works as &$work){
                                WorkItem::updateAll(['isDeleted' => 1], ['idWork'=>$work['idWork']]);
                            }
                        }
                    }
                }
                if (Selected::find()->where(['idCourse'=>$course['idCourse']])->exists()){
				    $slcts = Selected::find()->where(['idCourse'=>$course['idCourse']])->all();
					foreach($slcts as &$slct){
						Selected::updateAll(['isDeleted' => 1], ['idSelected'=>$slct['idSelected']]);
					}
				}
                return $this->redirect(array('auth/mainpage'));
            }
            if(Yii::$app->request->post('edit_course_course_'.$course['idCourse'])){
                return $this->redirect(array('editcourse','id'=>$course['idCourse']));
            }
            if(Yii::$app->request->post('open_report_course')){
                return $this->redirect(array('report/report','id'=>$course['idCourse']));
            }
            foreach($tasks as &$tsk){
                if(Yii::$app->request->post('open_task_course_'.$tsk->idTask)){
                    return $this->redirect(array('task/task','idtask'=>$tsk['idTask']));
                }
            }
            if(Yii::$app->request->post('select_course_'.$course['idCourse'])){
                if (Selected::find()->where(['idUser'=>$_SESSION['idUser'], 'idCourse'=>$course['idCourse'], 'IsDeleted'=>1])->exists()){
                    $sc = Selected::find()->where(['idUser'=>$_SESSION['idUser'], 'idCourse'=>$course['idCourse'], 'IsDeleted'=>1])->one();
                    $sc->IsDeleted = 0;
                    $sc->save();
                    return $this->refresh();
                } else {
                    $sc = new Selected();
                    $sc->idCourse = $course['idCourse'];
                    $sc->idUser = $_SESSION['idUser'];
                    $sc->IsDeleted = 0;
                    $sc->save();
                    return $this->refresh();
                }

            }
            if(Yii::$app->request->post('unselect_course_'.$course['idCourse'])){
                $sc = Selected::find()->where(['idUser'=>$_SESSION['idUser'], 'idCourse'=>$course['idCourse'], 'IsDeleted'=>0])->one();
                $sc->IsDeleted = 1;
                $sc->save();
                return $this->refresh();
            }

            return $this->render("course", compact('course', 'tasks', 'task_exist', 'versions','color', 'percent'));
        }
        else{
			return $this->redirect(['auth/authr']);
		}	
    }
    
    public function actionEditcourse($id = null){
        if (isset($_SESSION['auth'])){
            if(!isset($id)){
                return $this->redirect(["auth/mainpage"]);
            }
            $form = new EditCourseForm();
            $course = Course::findOne($id);
            $task_exist = Task::find()->where(['idCourse'=>$course['idCourse'], 'isDeleted'=>0])->exists();
            $tasks = Task::find()->where(['idCourse'=>$course['idCourse'], 'isDeleted'=>0])->all();

            if(Yii::$app->request->post('delete_course_editcourse_'.$course['idCourse'])){
                $course -> isDeleted = 1;
                $course -> save();
                if (Task::find()->where(['idCourse'=>$course['idCourse']])->exists()){
                    $tasks = Task::find()->where(['idCourse'=>$course['idCourse']])->all();
                    Task::updateAll(['isDeleted' => 1], ['idCourse'=>$course['idCourse']]);
                    foreach($tasks as &$task){
                        TaskItem::updateAll(['isDeleted' => 1], ['idTask'=>$task['idTask']]);
                        if(Work::find()->where(['idTask'=>$task['idTask']])->exists()){
                            $works = Work::find()->where(['idTask'=>$task['idTask']])->all();
                            Work::updateAll(['isDeleted' => 1], ['idTask'=>$task['idTask']]);
                            foreach($works as &$work){
                                WorkItem::updateAll(['isDeleted' => 1], ['idWork'=>$work['idWork']]);
                            }
                        }
                    }
                }
                return $this->redirect(["auth/mainpage"]);
            }

            if(Yii::$app->request->post('add_lab_'.$course['idCourse'])){
                $task = new Task();
                $task->idCourse = $course['idCourse'];
                $task->Task = "Название задания";
                $task->Comment = "Описание";
                $task->Uniqueness = 0;
                $task->IsDeleted = 0;
                $task->Year = 3;
                $task->Tries = 0;
                //$task->filesQuantity = 0;
                $task->save();
                if (!file_exists(yii::$app->basePath.'/uploads/' . $task->idCourse . '/' . $task->idTask))
                {
                    mkdir(yii::$app->basePath.'/uploads/' . $task->idCourse . '/' . $task->idTask);
                }

                if (!file_exists(yii::$app->basePath.'/uploads/' . $task->idCourse . '/' . $task->idTask . '/teacher_files'))
                {
                    mkdir(yii::$app->basePath.'/uploads/' . $task->idCourse . '/' . $task->idTask . '/teacher_files');
                }

                return $this->redirect(array('task/edittask','idtask'=>$task['idTask']));;
            }

            if(Yii::$app->request->post('go_to_the_course')){
                return $this->redirect(array('course', 'id'=>$id));
            }

            if($form->load(Yii::$app->request->post()) && Yii::$app->request->post('save_course_editcourse_'.$course['idCourse']) && $form->validate()){
                $course->Name = Html::encode($form->course_name);
                $course->Info = Html::encode($form->course_info);
                $course ->save();
                //return $this->redirect(array('course'));
                return $this->redirect(array('course/course','id'=>$course['idCourse']));
            }


            foreach($tasks as &$tsk){
                if(Yii::$app->request->post('open_task_editcourse_'.$tsk->idTask)){
                    return $this->redirect(array('task/task','idtask'=>$tsk->idTask));
                }
            }


            return $this->render("edit_course", compact('form','course', 'tasks', 'task_exist'));        
        }
        else{
			return $this->redirect(['auth/authr']);
		}	
    }

    public function actionRecovercourse(){
        

        $Courses = Course::find()->where(['isDeleted' => 1, 'idUser' => $_SESSION['idUser']])->all();

        if(Yii::$app->request->post('go_to_the_mainpage')){
            return $this->redirect(array('auth/mainpage'));
        }

        foreach($Courses as &$row){
            if(Yii::$app->request->post('recover_'.$row['idCourse'])){
                $row->isDeleted = 0;
                $row -> save();
                if (Task::find()->where(['idCourse'=>$row['idCourse']])->exists()){
                    $tasks = Task::find()->where(['idCourse'=>$row['idCourse']])->all();
                    Task::updateAll(['isDeleted' => 0], ['idCourse'=>$row['idCourse']]);
                    foreach($tasks as &$task){
                        TaskItem::updateAll(['isDeleted' => 0], ['idTask'=>$task['idTask']]);
                        //if(Work::find()->where(['idTask'=>$task['idTask']])->exists()){
                        //    $works = Work::find()->where(['idTask'=>$task['idTask']])->all();
                        //    Work::updateAll(['isDeleted' => 0], ['idTask'=>$task['idTask']]);
                        //    foreach($works as &$work){
                        //        WorkItem::updateAll(['isDeleted' => 0], ['idWork'=>$work['idWork']]);
                        //    }
                        //}
                    }
                }
                if (Selected::find()->where(['idCourse'=>$row['idCourse']])->exists()){
                    $slcts = Selected::find()->where(['idCourse'=>$row['idCourse']])->all();
                    foreach($slcts as &$slct){
                        Selected::updateAll(['isDeleted' => 0], ['idSelected'=>$slct['idSelected']]);
                    }
                }
                return $this->redirect(array('course','id'=>$row['idCourse']));
            }
        }

        return $this->render("recovercourse", compact('Courses'));
    }
}