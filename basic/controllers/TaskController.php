<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\AuthForm;
use app\models\EditTaskForm;
use app\models\EditTaskItemForm;
use app\models\TblUser;
use app\models\Task;
use app\models\TaskItem;
use app\models\Work;
use app\models\WorkItem;
use yii\data\Pagination;
use yii\widgets\LinkPager;
use yii\helpers\Html;
use app\models\Course;
use app\models\UploadFile;
use yii\web\UploadedFile;

class TaskController extends Controller
{
	public $output = '';
	public function actionTask($idtask=null){
		if (isset($_SESSION['auth'])){
			if(!isset($idtask)){
                return $this->redirect(["auth/mainpage"]);
            }
			$usr = TblUser::find()->where(['login'=>$_SESSION['auth']])->one();

			$task = Task::findOne($idtask);
			
			$taskItems_exist = TaskItem::find()->where(['idTask'=>$task['idTask'], 'isDeleted'=>0])->exists();
			$taskItems = TaskItem::find()->where(['idTask'=>$task['idTask'], 'isDeleted'=>0])->all();
			$taskItemsCount = TaskItem::find()->where(['idTask'=>$task['idTask'], 'isDeleted'=>0])->count();

			$uf = new UploadFile();
			if (Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'IsDeleted'=>0])->exists()){
				$max_vers = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'IsDeleted'=>0])->max('Version');
			
				$work_exist = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'IsDeleted'=>0, 'Version'=>$max_vers])->exists();
				$work = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'IsDeleted'=>0, 'Version'=>$max_vers])->one();
				if ($work_exist){
					$workItems_exist = WorkItem::find()->where(['idWork'=>$work['idWork'], 'IsDeleted'=>0])->exists();
            		$workItems = WorkItem::find()->where(['idWork'=>$work['idWork'], 'IsDeleted'=>0])->all();
				}
				else{
					$workItems_exist = false;
					$workItems = null;
				}
			}
			else{
				$work_exist = false;
				$workItems_exist = false;
				$workItems = null;
			}
            if(Yii::$app->request->post('go_to_the_course')){
                return $this->redirect(array('course/course', 'id'=>$task['idCourse']));
            }
			if(Yii::$app->request->post('delete_task_task_'.$task['idTask'])){
                $task -> IsDeleted = 1;
                $task -> save();
				TaskItem::updateAll(['isDeleted' => 1], ['idTask'=>$task['idTask']]);
				if(Work::find()->where(['idTask'=>$task['idTask']])->exists()){
					$works = Work::find()->where(['idTask'=>$task['idTask']])->all();
					Work::updateAll(['isDeleted' => 1], ['idTask'=>$task['idTask']]);
					foreach($works as &$work){
						WorkItem::updateAll(['isDeleted' => 1], ['idWork'=>$work['idWork']]);
					}
				}
                return $this->redirect(['course/course', 'id'=>$task->idCourse]);
            }
            if(Yii::$app->request->post('edit_task_task_'.$task['idTask'])){
                return $this->redirect(array('edittask','idtask'=>$idtask));
            }
			if(Yii::$app->request->post('upload_taskItems'.$task['idTask']))
			{
				$vers = 0;
				$work_exist = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'IsDeleted'=>0])->exists();
				if ($work_exist)
				{
					$vers = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'IsDeleted'=>0])->max('Version');
	    		}
				if (($task['Tries'] == 0 || $task['Tries'] > $vers) && $uf->validate())
				{
					$new_vers = $vers + 1;
					if (!file_exists(\yii::$app->basePath.'/uploads/' . $task->idCourse . '/' . $task->idTask . '/' . $_SESSION['auth']))
					{
						mkdir(\yii::$app->basePath.'/uploads/' . $task->idCourse . '/' . $task->idTask . '/' . $_SESSION['auth']);
					}
					if (!file_exists(\yii::$app->basePath.'/uploads/' . $task->idCourse . '/' . $task->idTask . '/' . $_SESSION['auth'] . '/' . $new_vers))
					{
						mkdir(\yii::$app->basePath.'/uploads/' . $task->idCourse . '/' . $task->idTask . '/' . $_SESSION['auth']. '/' . $new_vers);
					}

					$w = new Work();
					$w->idTask = $task['idTask'];
					$w->idUser = $usr['idUser'];
					$w->Date = date('Y-m-d');
					$w->Uniqueness = -1;
					$w->Version = $new_vers;
					$w->IsDeleted = 0;
					$w->save();

					$w1 = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'IsDeleted'=>0, 'Version'=>$new_vers])->one();
					
	    				for ($i = 0; $i < $taskItemsCount; $i++)
					{
						$ti = $taskItems[$i];
						$file = UploadedFile::getInstance($uf, 'file1['.$i.']');
						if ($file == null /*|| $file->extension != 'php'*/)
						{
							if($file == null){
								$_SESSION['error'] = "Пустой файл";
							}
							//else{
							//	$_SESSION['error'] = 'Можно загружать только php файлы';
							//}
							$w1->isDeleted = 1;
							$w1->save();
						}
						else
						{
							$filename = $uf->upload($i, $task['idCourse'], $task['idTask'], $_SESSION['auth'], $new_vers, $ti['Num'], $file);
	    						if($filename != false)
							{
								$wi = new WorkItem();
								$wi->idWork = $w1['idWork'];
								$wi->Num = $ti['Num'];
								$wi->File = $filename;
								$wi->Uniqueness = -1;
								$wi->IsDeleted = 0;
								$wi->save();
	    						}
							else
							{
								$output = 'Не удалось загрузить файл(ы)';
								$w1->IsDeleted = 1;
								$w1->save();

							}
						}
					}

					if ($w1['IsDeleted'] == 0)
					{
						$output = '';
						if(isset($_SESSION['status'])){
							$this->checkUniqueness($w1['idWork']);
						}
					}
					return $this->refresh();
				}
				else{
					$_SESSION['error'] ="Количество попыток превышен";
				}
			}
			else{
				if(!isset($output)){
					$output = '';
				}
			}
			if($workItems_exist){
				foreach($workItems as &$item)
				{
					if(Yii::$app->request->post('download_file_'.$item['idWorkItem']))
					{
						$w = Work::findOne($item['idWork']);
						$path = __DIR__ . '/../uploads/' . $task['idCourse'] . '/' . $task['idTask'] . '/' . $_SESSION['auth'] . '/' . $w['Version'] . '/';
						$file = $path . $item['File'];

						if (file_exists($file)) 
						{
							return \Yii::$app->response->sendFile($file);
						} 
						throw new \Exception('File not found');
					}
				}
			}


	        if(Yii::$app->request->post('open_report_task')){
                return $this->redirect(array('report/reporttask','idTask'=>$task['idTask']));
            }
			return $this->render('task', ['task'=>$task, 'output'=>$this->output, 'taskItems_exist'=>$taskItems_exist, 'taskItems'=>$taskItems, 'work_exist'=>$work_exist, 'workItems'=>$workItems, 'workItems_exist'=>$workItems_exist, 'uf'=>$uf]);
		}
		else{
			return $this->redirect(['auth/authr']);
		}
	}
	public function actionEdittask($idtask=null){
		if (isset($_SESSION['auth'])){
			$task = Task::findOne($idtask);
			$form = new EditTaskForm();
			$taskItems_exist = TaskItem::find()->where(['idTask'=>$task['idTask'], 'isDeleted'=>0])->exists();
            		$taskItems = TaskItem::find()->where(['idTask'=>$task['idTask'], 'isDeleted'=>0])->all();

			//$usr = TblUser::find()->where(['login'=>$_SESSION['auth']])->one();

			//$max_vers = Work::find()->where(['idTask'=>$task['idTask']])->andWhere(['idUser'=>$usr['idUser']])->max('Version');
			
			//$work_exist = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'Version'=>$max_vers])->exists();
			//$work = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'Version'=>$max_vers])->one();
			//if ($work_exist){
			//	$workItems_exist = WorkItem::find()->where(['idWork'=>$work['idWork']])->exists();
            		//	$workItems = WorkItem::find()->where(['idWork'=>$work['idWork']])->all();
			//}
			//else{
			//	$workItems_exist = false;
			//	$workItems = null;
			//}
			if(Yii::$app->request->post('go_to_the_task')){
                return $this->redirect(array('task', 'idtask'=>$idtask));
            }
			if(Yii::$app->request->post('add_item_'.$task['idTask'])){
				$fnum = TaskItem::find()->where(['idTask'=>$task['idTask'], 'isDeleted'=>0])->max('num');
                		$item = new TaskItem();
                		$item->idTask = $task['idTask'];
                		$item->Num = $fnum+1;
                		$item->Comment = "Файл подзадания";
                		$item->IsDeleted = 0;
                		if($item->save()){
					//$task['filesQuantity'] = $task['filesQuantity']+1;
					$task->save();
				}

				if (!file_exists(\yii::$app->basePath.'/uploads/' . $task->idCourse . '/' . $task->idTask . '/teacher_files'))
				{
					mkdir(\yii::$app->basePath.'/uploads/' . $task->idCourse . '/' . $task->idTask . '/teacher_files');
				}


				//return $this->refresh(); 
				$this->redirect(['edittaskitem','idtaskitem'=>$item['idTaskItem']]);
            }


			if($form->load(Yii::$app->request->post()) && Yii::$app->request->post('save_task_edittask_'.$task['idTask']) && $form->validate()){
                		$task->Task = Html::encode($form->task_name);
                		$task->Comment = Html::encode($form->task_comment);
                		$task->Tries = Html::encode($form->task_tries);
                		$task->Year = Html::encode($form->task_years);
                		$task->Uniqueness = Html::encode($form->task_uniqueness);
                		$task->save();
                		return $this->redirect(['task', 'idtask'=>$idtask]);
            		}
			if(Yii::$app->request->post('delete_task_edittask_'.$task['idTask'])){
                		$task->IsDeleted = 1;
                		$task->save();
						TaskItem::updateAll(['isDeleted' => 1], ['idTask'=>$task['idTask']]);
						if(Work::find()->where(['idTask'=>$task['idTask']])->exists()){
							$works = Work::find()->where(['idTask'=>$task['idTask']])->all();
							Work::updateAll(['isDeleted' => 1], ['idTask'=>$task['idTask']]);
							foreach($works as &$work){
								WorkItem::updateAll(['isDeleted' => 1], ['idWork'=>$work['idWork']]);
							}
						}
                		return $this->redirect(['course/course', 'id'=>$task->idCourse]);
            		}
			foreach($taskItems as &$item){
                		if(Yii::$app->request->post('open_item_edittask_'.$item->idTaskItem)){
                    			return $this->redirect(['edittaskitem','idtaskitem'=>$item->idTaskItem]);
                		}
            		}



			return $this->render('edit_task', ['task'=>$task, 'form'=>$form, 'taskItems_exist'=>$taskItems_exist, 'taskItems'=>$taskItems/*, 'work_exist'=>$work_exist, 'workItems'=>$workItems, 'workItems_exist'=>$workItems_exist*/]);
		}
		else{
			//return $this->redirect(['auth/authr']);
			return $this->render('task', ['task'=>$task, 'output'=>$this->output, 'taskItems_exist'=>$taskItems_exist, 'taskItems'=>$taskItems, 'work_exist'=>$work_exist, 'workItems'=>$workItems, 'workItems_exist'=>$workItems_exist, 'uf'=>$uf]);
		
		}
	}

	public function actionEdittaskitem($idtaskitem=null){
		if (isset($_SESSION['auth'])){
			$item = TaskItem::findOne($idtaskitem);
			$form = new EditTaskItemForm();

			if($form->load(Yii::$app->request->post()) && Yii::$app->request->post('save_taskitem_edittaskitem_'.$item['idTaskItem']) && $form->validate()){
                		$item->Comment = Html::encode($form->item_comment);
                		$item->save();
                		return $this->redirect(['edittask', 'idtask'=>$item['idTask']]);
            		}
			if(Yii::$app->request->post('delete_taskitem_edittaskitem_'.$item['idTaskItem'])){
                		$item->IsDeleted = 1;
                		$item->save();
				$task = Task::findOne($item['idTask']);
				//$task['filesQuantity'] = $task['filesQuantity']-1;
				$task->save();
                		return $this->redirect(['edittask', 'idtask'=>$item->idTask]);
            		}

			return $this->render('edit_taskitem', ['item'=>$item, 'form'=>$form]);
		}
		else{
			return $this->redirect(['auth/authr']);
		}
	}


	public function checkUniqueness($idWork)
    	{
		$work = Work::findOne(['idWork'=>$idWork]);
		$user = TblUser::findOne(['idUser'=>$work['idUser']]);
		$task = Task::findOne(['isDeleted'=>0, 'idTask'=>$work['idTask']]);
		$wis = WorkItem::find()->where(['isDeleted'=>0, 'idWork'=>$idWork])->all();
		
		//var_dump($user['login']);
		//var_dump($task['task']);
		
		$sum = 0;
		$old = getdate(mktime(0, 0, 0, date('m'), date('d'), date('Y') - $task['year']));
		$olddate = $old['year'].'-'.$old['mon'].'-'.$old['mday'];
		//$count = WorkItem::find()->where(['>','date',$old])->andWhere(['isDeleted'=>0, 'idWork'=>$idWork])->count();
		$count = WorkItem::find()->where(['isDeleted'=>0, 'idWork'=>$idWork])->count();
		//var_dump($count);

		if($count <= 0)
		{
			return false;
		}

		$min_unic = [];
		for($i = 0; $i < $count; $i++)
		{	
			$change = $wis[$i];
			$min_unic[$change['idWorkItem']] = 100;
			//var_dump($min_unic[$change['idWorkItem']]);
		}
		//$check_ws = Work::find()->where(['idTask'=>$work['idTask'], 'isDeleted'=>0])->all();
		$check_ws = Work::find()->where(['>','date',$olddate])->andWhere(['idTask'=>$work['idTask'], 'isDeleted'=>0])->all();
		foreach ($check_ws as &$check_w)
		{
			$check_u = TblUser::findOne(['idUser'=>$check_w['idUser']]);
			//var_dump($check_u['login']);
			if($check_u['idUser'] != $user['idUser'])
			{
				//$checks = WorkItem::find()->where(['isDeleted'=>0, 'idWork'=>$check_w['idWork']])->all();

				$check_count = WorkItem::find()->where(['isDeleted'=>0, 'idWork'=>$check_w['idWork']])->count();

				if($check_count == $count)
				{
					foreach ($wis as &$wi)
					{
						$check = WorkItem::findOne(['isDeleted'=>0, 'idWork'=>$check_w['idWork'], 'num'=>$wi['num']]);
						if($check != null)
						{
							$wi_path = yii::$app->basePath . '/uploads/' . $task['idCourse'] . '/' . $work['idTask'] . '/' . $user['login'] . '/' . $work['version'] . '/' . $wi['file'];
							$check_path = yii::$app->basePath . '/uploads/' . $task['idCourse'] . '/' . $work['idTask'] . '/' . $check_u['login'] . '/' . $check_w['version'] . '/' . $check['file'];
							if (file_exists($wi_path) && file_exists($check_path))	
							{
								$cmd = "python3 ".Yii::$app->basePath."/common/python/antiplagiat.py $wi_path $check_path";
								//var_dump($cmd);
								$res = shell_exec(escapeshellcmd($cmd));
								//var_dump($res);
								if ($res < $min_unic[$wi['idWorkItem']])
								{
									$min_unic[$wi['idWorkItem']] = $res;
								}
							}
						}
					}
	
				}

			}
		}

		foreach($wis as &$wi)
		{
			//var_dump($wi);
			$wi->uniqueness = $min_unic[$wi['idWorkItem']];
			$wi->save();
			$sum += floatval($min_unic[$wi['idWorkItem']]);
			//var_dump($min_unic[$wi['idWorkItem']]);
		}

		$unic = $sum / $count;
		$work->uniqueness = $unic;
		$work->save();
		//var_dump($unic);
		//die();
		
		
    	}

}
