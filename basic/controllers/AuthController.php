<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\AuthForm;
use app\models\User;
use yii\data\Pagination;
use yii\widgets\LinkPager;
use yii\helpers\Html;
use app\models\Selected;
use app\models\Course;
use app\models\TblUser;
use app\models\TaskItem;
use app\models\Work;
use app\models\WorkItem;
use app\models\Task;

class AuthController extends Controller
{

	public function actionExit(){
		if(session_status() === PHP_SESSION_ACTIVE){
			session_destroy();
		}
		return $this->redirect(['authr']);
	}
	public function actionAuthr()
    	{	
		if(session_status() === PHP_SESSION_ACTIVE){
    		session_destroy();
		}
		//session_destroy();
		$form = new AuthForm();
		
		if ($form->load(Yii::$app->request->post())){
			$out = $form->login();
			$login = Html::encode($form->login);
			if ($out){
				$output = explode( ' ', $form->output);
				//var_dump($output);die();
				if ($output[0] = 'True'){
					session_start();
					//закоменчено для вида от препода
					//if($output[1] == "True"){
					//	$_SESSION['status'] = true;
					//}
					if(!TblUser::find()->where(['Login'=>$login])->exists()){
						$usr = new TblUser();
						$usr -> Login = $login;
						if($output[1] == 'False'){
							$usr -> IsTeacher = 1;
						}
						else{
							$usr -> IsTeacher = 0;
						}
						$usr -> FIO = $output[2].' '.$output[3].' '.$output[4];
						$usr->save();
					}		
					$usr = TblUser::find()->where(['login' => $login])->one();
					$_SESSION['idUser'] = $usr['idUser'];

					$_SESSION['mpFlag'] = true;
					$_SESSION['auth'] = $login;
					$_SESSION['name'] = $output[2].' '.$output[3].' '.$output[4];
					return $this->redirect(["mainpage"]);
				}
			}
		}
		else{
			$output = '1';
		}
		return $this->render('authr', compact('form'));
        }
	public function actionMainpage(){
	       if (isset($_SESSION['auth'])){
			if($_SESSION['mpFlag']){
				$Courses = Course::find()->where(['isDeleted' => 0])->all();
			}
			else{
				$selCourses = Selected::find()->where(['idUser' => $_SESSION['idUser'], 'IsDeleted'=>0])->all();
				$Courses = array();
				foreach ($selCourses as $SC){
					array_push($Courses, Course::find()->where(['idCourse' => $SC['idCourse']])->one());
				}
			}
			if(Yii::$app->request->post('addCourse')){
				$course = new Course();
				$course -> Name = "Название курса";
				$course -> Info = "Описание курса";
				$course -> idUser = $_SESSION['idUser'];
				$course -> isDeleted = 0;
				$course -> save();
				if (!file_exists( yii::$app->basePath.'/uploads/' . $course->idCourse))
				{
					mkdir(yii::$app->basePath.'/uploads/' . $course->idCourse);
				}
				$courses = Course::find()->where(['isDeleted' => 0])->all();
				foreach ($courses as $crs)
				{
					if (!Selected::find()->where(['idCourse' => $crs['idCourse'], 'idUser' => $_SESSION['idUser']])->exists())
					{
						$selected = new Selected();
						$selected -> idCourse = $crs['idCourse'];
						$selected -> idUser = $_SESSION['idUser'];
						$selected -> save();
					}
				}
				return $this->redirect(array('course/editcourse','id' => $course['idCourse']));
			}
			if(Yii::$app->request->post('recoverCourse')){
				return $this->redirect(['course/recovercourse']);
			}
			if(count($Courses) > 0){
				foreach($Courses as &$row){
					if(Yii::$app->request->post('delete_course_main_'.$row['idCourse'])){
						$row->isDeleted = 1;
						$row -> save();
						if (Task::find()->where(['idCourse'=>$row['idCourse']])->exists()){
							$tasks = Task::find()->where(['idCourse'=>$row['idCourse']])->all();
							Task::updateAll(['IsDeleted' => 1], ['idCourse'=>$row['idCourse']]);
							foreach($tasks as &$task){
								TaskItem::updateAll(['IsDeleted' => 1], ['idTask'=>$task['idTask']]);
								if(Work::find()->where(['idTask'=>$task['idTask']])->exists()){
									$works = Work::find()->where(['idTask'=>$task['idTask']])->all();
									Work::updateAll(['IsDeleted' => 1], ['idTask'=>$task['idTask']]);
									foreach($works as &$work){
										WorkItem::updateAll(['IsDeleted' => 1], ['idWork'=>$work['idWork']]);
									}
								}
							}
						}
						if (Selected::find()->where(['idCourse'=>$row['idCourse']])->exists()){
							$slcts = Selected::find()->where(['idCourse'=>$row['idCourse']])->all();
							foreach($slcts as &$slct){
								Selected::updateAll(['isDeleted' => 1], ['idSelected'=>$slct['idSelected']]);
							}
						}
						return $this -> refresh();
					}
					else if(Yii::$app->request->post('edit_course_main_'.$row['idCourse'])){
						return $this->redirect(array('course/editcourse','id' => $row['idCourse']));
					}
					else if(Yii::$app->request->post('open_course_main_'.$row['idCourse'])){
						return $this->redirect(array('course/course', 'id'=>$row['idCourse']));
					}
					else if(Yii::$app->request->post('showAllCourse')){
						$_SESSION['mpFlag'] = true;
						return $this -> refresh();
					}
					else if(Yii::$app->request->post('showSelectedCourse')){
						$_SESSION['mpFlag'] = false;
						return $this -> refresh();
					}
				}
				return $this->render("main_page", compact('Courses'));
			}
		}
		else{
			return $this->redirect(['authr']);
		}	
	       
	}
}
