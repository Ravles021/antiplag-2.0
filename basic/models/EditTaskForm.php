<?php
namespace app\models;

use Yii;
use yii\base\Model;

class EditTaskForm extends Model
{
	public $task_name;
	public $task_comment;
	public $task_uniqueness;
	public $task_tries;
	public $task_years;

	public function rules()
	{
		return [
		       [['task_name', 'task_comment', 'task_uniqueness', 'task_tries', 'task_years'],'required'],
		];
	}
}
?>
