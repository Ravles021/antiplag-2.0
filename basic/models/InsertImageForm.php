<?php
namespace app\models;

use Yii;
use yii\base\Model;

class InsertImageForm extends Model
{
	public $name;
	public $caption;

	public function rules()
	{
		return [
		       [['name','caption'],'required','message'=>'Поле не должно быть пустым'],
		];
	}
}
?>
