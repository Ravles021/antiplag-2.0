<?php
namespace app\models;

use Yii;
use yii\base\Model;

class DatesForm extends Model
{
	public $start_date;
	public $end_date;

	public function rules()
	{
		return [
			['start_date', 'date', 'format' => 'php:Y-m-d'],
			['end_date', 'date', 'format' => 'php:Y-m-d']
		];
	}
}
?>
