<?php
$this->title="Главная страница";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<h1>Курсы</h1><br>
<header>
<?php if(!isset($_SESSION['status'])){ ?>
	<?php $f = ActiveForm::begin() ?>
    	<?= Html::submitButton('Добавить курс',['name'=>'addCourse', 'value' => 'add', 'class' => 'btn btn-primary']);?>
		<?php if(!$_SESSION['mpFlag']) {?>
    	<?= Html::submitButton('Показать все',['name'=>'showAllCourse', 'value' => 'add', 'class' => 'btn btn-primary']);?>
		<?php } else{?>
		<?= Html::submitButton('Показать избранные',['name'=>'showSelectedCourse', 'value' => 'add', 'class' => 'btn btn-primary']);?>
		<?php } ?>
		<?= Html::submitButton('Восстановление',['name'=>'recoverCourse', 'value' => 'add', 'class' => 'btn btn-primary']);?>
	<?php ActiveForm::end() ?>
<?php } ?>
<?php if(isset($_SESSION['status'])){ ?>
	<?php $f = ActiveForm::begin() ?>
    	<?php if(!$_SESSION['mpFlag']) {?>
    	<?= Html::submitButton('Показать все',['name'=>'showAllCourse', 'value' => 'add', 'class' => 'btn btn-primary']);?>
		<?php } else{?>
		<?= Html::submitButton('Показать избранные',['name'=>'showSelectedCourse', 'value' => 'add', 'class' => 'btn btn-primary']);?>
		<?php } ?>
	<?php ActiveForm::end() ?>
<?php } ?>
</header>
<div id="courses-scroll" style="overflow: auto; margin-left: 50px; height: 400px;">
<?php $f = ActiveForm::begin() ?>
	<?php
	foreach($Courses as &$row){
    	echo '<div class="one-course">

    	<div class="col-log-3">
    	'.Html::submitButton($row['Name'],['name'=>'open_course_main_'.$row['idCourse'], 'value' => 'add', 'class' => 'submit_text']).'<br><br>';
		if(!isset($_SESSION['status'])){
			if($row['idUser'] == $_SESSION['idUser']){
				echo Html::submitButton('Редактировать',['name'=>'edit_course_main_'.$row['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary']).'<br><br>
				'.Html::submitButton('Удалить',['name'=>'delete_course_main_'.$row['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary']);
			}
		}
		echo '</div>
    	</div><br>';
	}	
	?>
<?php ActiveForm::end() ?>
</div><br>
<div>
<a href="#" style="float:left" id="show-all" onclick="showAllCourses()">Показать все курсы</a><br><br>

</div>