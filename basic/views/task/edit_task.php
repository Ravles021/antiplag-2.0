<?php
$this->title="Страница редактирования задания";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Task;
use app\models\TaskItem;


?>
<?php $f = ActiveForm::begin() ?>
<?=Html::submitButton('☚ На страницу задания',['name'=>'go_to_the_task', 'value' => 'add', 'class' => 'btn btn-primary'])?><br><br><br>
<?php ActiveForm::end() ?>

<?php $f = ActiveForm::begin() ?>
    <?=$f->field($form, 'task_name')->textInput([ 'class'=>'input_edit element-inline','value'=>$task['Task'], 'placeholder'=>'Название задания'])->label(false)?>
    <?=$f->field($form, 'task_comment')->textArea([ 'class'=>'info_textarea','value'=>$task['Comment'], 'placeholder'=>'Описание'])->label(false)?>
    <hr>
    <h1>Settings</h1>
    <?=$f->field($form, 'task_uniqueness')->textInput(['onchange'=>'changeU()', 'id'=>'uniq', 'class'=>'input_edit element-inline','value'=>$task['Uniqueness'], 'placeholder'=>'Уникальность'])->label('Уникальность', ['class'=>'label-class'])?>
    <?=$f->field($form, 'task_tries')->textInput(['onchange'=>'changeT()', 'id'=>'tries', 'class'=>'input_edit element-inline','value'=>$task['Tries'], 'placeholder'=>'Кол-во попыток'])->label('Кол-во попыток', ['class'=>'label-class'])?>
    <?=$f->field($form, 'task_years')->textInput(['onchange'=>'changeY()', 'id'=>'year', 'class'=>'input_edit element-inline','value'=>$task['Year'], 'placeholder'=>'Период (в годах)'])->label('Период (в годах)', ['class'=>'label-class'])?>

    <?=Html::submitButton('Сохранить',['name'=>'save_task_edittask_'.$task['idTask'], 'value' => 'add', 'class' => 'btn btn-primary element-inline'])?>
    <?=Html::submitButton('Удалить задание',['name'=>'delete_task_edittask_'.$task['idTask'], 'value' => 'add', 'class' => 'btn btn-primary element-inline element-right'])?>

	<br><br>
    <div class ="laboratornye">
    <table class="table">
        <thead>
        <tr><th>Номер</th><th>Описание</th></tr>
        </thead>
        <tbody>
         <?php
            if ($taskItems_exist){
                foreach($taskItems as &$item){
                     	echo '<tr><td>'.$item['Num'].'</td><td>
                     		'.Html::submitButton($item->Comment,['name'=>'open_item_edittask_'.$item['idTaskItem'], 'value' => 'add', 'class' => 'submit_text']).'
                     		</td></tr>';
                }
	    }
         ?>
        </tbody>
    </table>
    <?=Html::submitButton('Добавить',['name'=>'add_item_'.$task['idTask'], 'value' => 'add', 'class' => 'btn btn-primary element-inline'])?>
    </div>
    <br><br>
<?php ActiveForm::end() ?>

<script>
    var uniq = document.getElementById('uniq');
    var year = document.getElementById('year');
    var tries = document.getElementById('tries');


    function changeU()
    {
        if(uniq.value > 100)
        {
            uniq.value = 100;
            alert("Процент уникальности не может быть больше 100");
        }
        if(uniq.value < 0)
        {
            uniq.value = 0;
            alert("Процент уникальности не может быть меньше 0");
        }
        if(!parseInt(uniq.value))
        {
            uniq.value = 0;
            alert("Процент уникальности должен быть числом");
        }
    } 
    function changeY()
    {
        if(year.value < 0)
        {
            year.value = 0;
            alert("Количество лет не может быть меньше 0");
        }
        if(!parseInt(year.value))
        {
            year.value = 0;
            alert("Количество лет должено быть числом");
        }
    } 
    function changeT()
    {
        if(tries.value < 0)
        {
            tries.value = 0;
            alert("Количество попыток не может быть меньше 0");
        }
        if(!parseInt(tries.value))
        {
            tries.value = 0;
            alert("Количество попыток должено быть числом");
        }
    } 
</script>