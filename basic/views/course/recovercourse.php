<?php
$this->title="Восстановление";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<h1>Курсы</h1><br>
<header>
	<?=Html::submitButton('☚ На главную',['name'=>'go_to_the_mainpage', 'value' => 'add', 'class' => 'btn btn-primary'])?><br><br><br>
</header>
<div id="courses-scroll" style="overflow: auto; margin-left: 50px; height: 400px;">
<?php $f = ActiveForm::begin() ?>
	<?php
	foreach($Courses as &$row){
    	echo '<div class="one-course">

    	<div class="col-log-3">
    	'.Html::submitButton($row['Name'],['name'=>'open_course_main_'.$row['idCourse'], 'value' => 'add', 'class' => 'submit_text']).'<br><br>';
		if(!isset($_SESSION['status'])){
			if($row['idUser'] == $_SESSION['idUser']){
				echo Html::submitButton('Восстановить',['name'=>'recover_'.$row['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary']).'<br>';
			}
		}
		echo '</div>
    	</div><br>';
	}	
	?>
<?php ActiveForm::end() ?>
</div><br>
<div>
<a href="#" style="float:left" id="show-all" onclick="showAllCourses()">Показать все курсы</a><br><br>

</div>