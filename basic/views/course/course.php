<?php
$this->title="Страница курса";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Selected;
?>
<?php $f = ActiveForm::begin() ?>
<?=Html::submitButton('☚ На главную',['name'=>'go_to_the_mainpage', 'value' => 'add', 'class' => 'btn btn-primary'])?><br><br><br>
<?php ActiveForm::end() ?>
<div>
    <p class="element-inline"><?= $course["Name"] ?></p>
    <?php $f = ActiveForm::begin() ?>
    <?php if(!isset($_SESSION['status'])){?>
        <?php if($course['idUser'] == $_SESSION['idUser']){?>    
        
        <?=Html::submitButton('Редактировать курс',['name'=>'edit_course_course_'.$course['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary element-inline element-right'])?>
        <?=Html::submitButton('Удалить курс',['name'=>'delete_course_course_'.$course['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary element-inline element-right right_margin'])?>
        <?php  } else{
            if (Selected::find()->where(['idUser'=>$_SESSION['idUser'], 'idCourse'=>$course['idCourse'], 'IsDeleted'=>0])->exists()){?>
            <?=Html::submitButton('Убрать из избранного',['name'=>'unselect_course_'.$course['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary element-inline element-right'])?>
            <?php } else {?>
            <?=Html::submitButton('Добавить в избранное',['name'=>'select_course_'.$course['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary element-inline element-right'])?>
         <?php }} ?>
        
    <?php } else { 
        if (Selected::find()->where(['idUser'=>$_SESSION['idUser'], 'idCourse'=>$course['idCourse'], 'IsDeleted'=>0])->exists()){?>
            <?=Html::submitButton('Убрать из избранного',['name'=>'unselect_course_'.$course['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary element-inline element-right'])?>
            <?php } else {?>
            <?=Html::submitButton('Добавить в избранное',['name'=>'select_course_'.$course['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary element-inline element-right'])?>
         <?php }} ?>
    <?php ActiveForm::end() ?>
	<br><br>
	<div class="info-style"><?= $course["Info"] ?></div> 
    <hr>
    <?php $f = ActiveForm::begin() ?>
    <table class="table">
         <?php
            if(!isset($_SESSION['status'])){ 
                echo '<thead>
                <tr><th>Лабораторные работы</th><th>Суммарное количество попыток</th><th>Процент сдавших</th></tr>
                </thead>
                <tbody>';
                if ($task_exist){
                    foreach($tasks as &$tsk){
                        echo '<tr><td>
                        '.Html::submitButton($tsk->Task,['name'=>'open_task_course_'.$tsk['idTask'], 'value' => 'add', 'class' => 'submit_text']).'
                        </td>
                        <td>'.$versions[$tsk['idTask']].'</td><td>'.$percent[$tsk['idTask']].'</td></tr>';
                    }
                }
            }
            else{
                echo '<thead>
                <tr><th>Лабораторные работы</th><th>Количество попыток</th></tr>
                </thead>
                <tbody>';
                if ($task_exist){
                    foreach($tasks as &$tsk){
                        echo '<tr><td>
                        '.Html::submitButton($tsk->Task,['name'=>'open_task_course_'.$tsk['idTask'], 'value' => 'add', 'class' => 'submit_text']).'
                        </td>
                        <td style="background-color:'.$color[$tsk['idTask']].';color:white;">'.$versions[$tsk['idTask']].'</td></tr>';
                    }
                }
            }
         ?>
        </tbody>
    </table><br><br>
    <?php if(!isset($_SESSION['status'])){?>
    <?=Html::submitButton('Получить отчёт',['name'=>'open_report_course', 'value' => 'add', 'class' => 'btn btn-primary element-right'])?>
    <?php }?>
    <?php ActiveForm::end() ?>
</div>
