<?php
$this->title="Отчёт";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>


<h1>Отчёт по студенту: <?= $usr["FIO"] ?></h1>

<?php $f = ActiveForm::begin() ?>
        
    <?= $f->field($form, 'start_date')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
        'value' => $start,
    ]) ?>
    <?= $f->field($form, 'end_date')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
        'value' => $end,
    ]) ?>
    <p><?php echo $filter_info ?></p>
    
    <?=Html::submitButton('Фильтровать',['name'=>'new_dates', 'value' => 'add', 'class' => 'btn btn-primary element-inline'])?>
    </div>
    <br><br>
<?php ActiveForm::end() ?>


<?php $f = ActiveForm::begin() ?>
<table class='table'>
        <thead>
            <tr><th>Курс</th><th>Задание</th><th>Номер попытки</th><th>Дата загрузки версии</th><th>Скачать версию</th><th>Результат</th></tr>
        </thead>
        <tbody>
        <?php
        	foreach ($works as &$work) {
                $work_date_str = explode("-", $work['Date']);
                $work_date = mktime(0, 0, 0, $work_date_str[1], $work_date_str[2], $work_date_str[0]);
                if ($sd === "" && $ed === "")
                {
        		    echo '<tr><td>'.$coursenames[$work['idTask']].'</td>
                    <td>'.$tasknames[$work['idTask']].'</td>
        		    <td>'.$work['Version'].'</td>
                    <td>'.$work['Date'].'</td>
                    <td>'.Html::submitButton('скачать',['name'=>'download_zip_'.$work['idWork'], 'value' => 'addafs', 'class' => 'submit_text']).'</td>
                    <td>'.$work['Uniqueness'].'</td></tr>';
                }
                if ($sd != "" && $ed != "")
                {
                    if ($work_date >= $sd && $work_date <= $ed)
                    {
                        echo '<tr><td>'.$coursenames[$work['idTask']].'</td>
                        <td>'.$tasknames[$work['idTask']].'</td>
        		        <td>'.$work['Version'].'</td>
                        <td>'.$work['Date'].'</td>
                        <td>'.Html::submitButton('скачать',['name'=>'download_zip_'.$work['idWork'], 'value' => 'addafs', 'class' => 'submit_text']).'</td>
                        <td>'.$work['Uniqueness'].'</td></tr>';
                    }
                }
                if ($sd != "" && $ed == "")
                {
                    if ($work_date >= $sd)
                    {
                        echo '<tr><td>'.$coursenames[$work['idTask']].'</td>
                        <td>'.$tasknames[$work['idTask']].'</td>
        		        <td>'.$work['Version'].'</td>
                        <td>'.$work['Date'].'</td>
                        <td>'.Html::submitButton('скачать',['name'=>'download_zip_'.$work['idWork'], 'value' => 'addafs', 'class' => 'submit_text']).'</td>
                        <td>'.$work['Uniqueness'].'</td></tr>';
                    }
                }
                if ($sd == "" && $ed != "")
                {
                    if ($work_date <= $ed)
                    {
                        echo '<tr><td>'.$coursenames[$work['idTask']].'</td>
                        <td>'.$tasknames[$work['idTask']].'</td>
        		        <td>'.$work['Version'].'</td>
                        <td>'.$work['Date'].'</td>
                        <td>'.Html::submitButton('скачать',['name'=>'download_zip_'.$work['idWork'], 'value' => 'addafs', 'class' => 'submit_text']).'</td>
                        <td>'.$work['Uniqueness'].'</td></tr>';
                    }
                }
        	}
        ?>
        </tbody>
</table>
<?php ActiveForm::end() ?>