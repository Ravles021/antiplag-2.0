<?php
$this->title="Отчёт";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $f = ActiveForm::begin() ?>
<?=Html::submitButton('☚ На страницу задания',['name'=>'go_to_the_task', 'value' => 'add', 'class' => 'btn btn-primary'])?><br><br><br>
<?php ActiveForm::end() ?>

<h1>Отчёт по заданию: <?= $tsk["Task"] ?></h1>

<?php $f = ActiveForm::begin() ?>
        
    <?= $f->field($form, 'start_date')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
        'value' => $start,
    ]) ?>
    <?= $f->field($form, 'end_date')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
        'value' => $end,
    ]) ?>
    
    <p><?php echo $filter_info ?></p>
    
    <?=Html::submitButton('Фильтровать',['name'=>'new_dates', 'value' => 'add', 'class' => 'btn btn-primary element-inline'])?>
    </div>
    <br><br>
<?php ActiveForm::end() ?>

<?php $f = ActiveForm::begin() ?>
<table class='table'>
        <thead>
            <tr><th>Студент</th><th>Номер попытки</th><th>Дата загрузки версии</th><th>Скачать версию</th><th>Результат</th></tr>
        </thead>
        <tbody>
        <?php
        	foreach ($works as &$work) {
                $work_date_str = explode("-", $work['Date']);
                $work_date = mktime(0, 0, 0, $work_date_str[1], $work_date_str[2], $work_date_str[0]);
                if ($sd === "" && $ed === "")
                {
        		    echo '<tr><td>'.Html::submitButton($usernames[$work['idUser']],['name'=>'open_report_by_name_'.$work['idUser'], 'value' => 'addafs', 'class' => 'submit_text']).'</td>
        		    <td>'.$work['Version'].'</td>
                    <td>'.$work['Date'].'</td>
                    <td>'.Html::submitButton('скачать',['name'=>'download_zip_'.$work['idWork'], 'value' => 'addafs', 'class' => 'submit_text']).'</td>
                    <td>'.$work['Uniqueness'].'</td></tr>';
                }
                if ($sd != "" && $ed != "")
                {
                    if ($work_date >= $sd && $work_date <= $ed)
                    {
                        echo '<tr><td>'.Html::submitButton($usernames[$work['idUser']],['name'=>'open_report_by_name_'.$work['idUser'], 'value' => 'addafs', 'class' => 'submit_text']).'</td>
        		        <td>'.$work['Version'].'</td>
                        <td>'.$work['Date'].'</td>
                        <td>'.Html::submitButton('скачать',['name'=>'download_zip_'.$work['idWork'], 'value' => 'addafs', 'class' => 'submit_text']).'</td>
                        <td>'.$work['Uniqueness'].'</td></tr>';
                    }
                }
                if ($sd != "" && $ed == "")
                {
                    if ($work_date >= $sd)
                    {
                        echo '<tr><td>'.Html::submitButton($usernames[$work['idUser']],['name'=>'open_report_by_name_'.$work['idUser'], 'value' => 'addafs', 'class' => 'submit_text']).'</td>
        		        <td>'.$work['Version'].'</td>
                        <td>'.$work['Date'].'</td>
                        <td>'.Html::submitButton('скачать',['name'=>'download_zip_'.$work['idWork'], 'value' => 'addafs', 'class' => 'submit_text']).'</td>
                        <td>'.$work['Uniqueness'].'</td></tr>';
                    }
                }
                if ($sd == "" && $ed != "")
                {
                    if ($work_date <= $ed)
                    {
                        echo '<tr><td>'.Html::submitButton($usernames[$work['idUser']],['name'=>'open_report_by_name_'.$work['idUser'], 'value' => 'addafs', 'class' => 'submit_text']).'</td>
        		        <td>'.$work['Version'].'</td>
                        <td>'.$work['Date'].'</td>
                        <td>'.Html::submitButton('скачать',['name'=>'download_zip_'.$work['idWork'], 'value' => 'addafs', 'class' => 'submit_text']).'</td>
                        <td>'.$work['Uniqueness'].'</td></tr>';
                    }
                }
        	}
        ?>
        </tbody>
</table>
<?php ActiveForm::end() ?>