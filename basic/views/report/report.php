<?php
$this->title="Отчёт";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $f = ActiveForm::begin() ?>
<?=Html::submitButton('☚ На страницу курса',['name'=>'go_to_the_course', 'value' => 'add', 'class' => 'btn btn-primary'])?><br><br><br>
<?php ActiveForm::end() ?>

<h1>Отчёт по курсу: <?= $course["Name"] ?></h1>

<?php $f = ActiveForm::begin() ?>
    
    <?= $f->field($form, 'start_date')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
        'value' => $start,
    ]) ?>
    <?= $f->field($form, 'end_date')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
        'value' => $end,
    ]) ?>


    <p><?php echo $filter_info ?></p>
    

    <?=Html::submitButton('Фильтровать',['name'=>'new_dates', 'value' => 'add', 'class' => 'btn btn-primary element-inline'])?>
    </div>
    <br><br>
<?php ActiveForm::end() ?>


<table class='table'>
        <thead>
            <tr><th>Задание</th><th>Студент</th><th>Дата</th><th>Количество попыток</th><th>Результат</th></tr>
        </thead>
        <tbody>
        <?php
            foreach($works as &$row){
                foreach($row as &$work){
                    $work_date_str = explode("-", $work['Date']);
                    $work_date = mktime(0, 0, 0, $work_date_str[1], $work_date_str[2], $work_date_str[0]);
                    if ($sd === "" && $ed === "")
                    {
                        echo '<tr><td>'.$tasknames[$work['idTask']].'</td><td>'.$usernames[$work['idUser']].'</td>
                        <td>'.$work['Date'].'</td><td>'.$work['Version'].'</td><td>'.$work['Uniqueness'].'</td></tr>';
                    }
                    if ($sd != "" && $ed != "")
                    {
                        if ($work_date >= $sd && $work_date <= $ed)
                        {
                            echo '<tr><td>'.$tasknames[$work['idTask']].'</td><td>'.$usernames[$work['idUser']].'</td>
                            <td>'.$work['Date'].'</td><td>'.$work['Version'].'</td><td>'.$work['Uniqueness'].'</td></tr>';
                        }
                    }
                    if ($sd != "" && $ed == "")
                    {
                        if ($work_date >= $sd)
                        {
                            echo '<tr><td>'.$tasknames[$work['idTask']].'</td><td>'.$usernames[$work['idUser']].'</td>
                            <td>'.$work['Date'].'</td><td>'.$work['Version'].'</td><td>'.$work['Uniqueness'].'</td></tr>';
                        }
                    }
                    if ($sd == "" && $ed != "")
                    {
                        if ($work_date <= $ed)
                        {
                            echo '<tr><td>'.$tasknames[$work['idTask']].'</td><td>'.$usernames[$work['idUser']].'</td>
                            <td>'.$work['Date'].'</td><td>'.$work['Version'].'</td><td>'.$work['Uniqueness'].'</td></tr>';
                        }
                    }
                }
            }
        ?>
        </tbody>
</table>
